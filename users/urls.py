from django.urls import path, include

from rest_framework.routers import DefaultRouter
from .views import (
    UserProfileViewSet,
    ScreenshotViewSet,
    UserSignup,
    UserLogin
)

router = DefaultRouter()
router.register(r'users', UserProfileViewSet)
router.register(r'screenshots', ScreenshotViewSet)

urlpatterns = [
    path('', include(router.urls)),
    
    path('signup/', UserSignup.as_view(), name="user-signup"),
    path('login/', UserLogin.as_view(), name="user-login"),
]
