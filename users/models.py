from django.db import models
from django.contrib.auth.models import User
from admin_panel.models import AndroidApp

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    points_earned = models.PositiveIntegerField(default=0)
    
class Screenshot(models.Model):
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    app = models.ForeignKey(AndroidApp, on_delete=models.CASCADE)
    screenshot = models.ImageField(upload_to="images/screenshots/")
