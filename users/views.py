from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken

from django.contrib.auth.models import User
from .models import UserProfile, Screenshot
from .serializers import UserProfileSerializers, ScreenshotSerializer



class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializers
    

class ScreenshotViewSet(viewsets.ModelViewSet):
    queryset = Screenshot.objects.all()
    serializer_class = ScreenshotSerializer
    
class UserSignup(APIView):
    def post(self, request):
        username = request.data.get("username")
        password = request.data.get("password")
        email = request.data.get("email")
        
        if User.objects.filter(username=username).exists():
            return Response({
                'detail': "Username already exists."
            }, status=status.HTTP_404_BAD_REQUEST)
        
        user = User.objects.create_user(username=username, password=password, email=email)
        
        token, created = Token.objects.get_or_create(user=user)
        
        return Response({
            'token': token.key,
            'user_id': user.id
        }, status=status.HTTP_201_CREATED)

class UserLogin(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        response = super(UserLogin, self).post(request, *args, **kwargs)
        
        if response.status_code == status.HTTP_200_OK:
            token_key = response.data["token"]
            user_id = Token.objects.get(key=token_key).user.id
            return Response({
                'token': token_key,
                'user_id': user_id
            }, status=status.HTTP_200_OK)
        
        return response