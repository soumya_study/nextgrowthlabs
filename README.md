# Problem Set 1 - Regex

1. Write a regex to extract all the numbers with orange color background from the below text in italics (Output should be a list).

{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}

### Solution

import re

text ='''

{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}

'''

regex =r'\{"id":\d+\}'

numbers_with_orange_background = re.findall(regex, text)

result =[int(re.search(r'\d+', match).group())for match in numbers_with_orange_background]

print(result)

# Problem Set 2 - A functioning web app with API

## NextLab Assignment API

The NextLab Assignment API is a Django-based RESTful API that facilitates the management of Android apps, user profiles, and rewards. It is designed to support a reward system where users earn points for downloading Android apps and completing tasks.

## Table of Contents

- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [API Endpoints](#api-endpoints)
- [Authentication](#authentication)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [License](#license)

## Features

- Admin-facing endpoints for adding Android apps and setting download points.
- User-facing endpoints for user registration, profile management, screenshot uploads, and app listings.
- Token-based authentication for both admin and regular users.
- Automatic API documentation generation using Swagger.

## Installation

1. Clone the repository:

   ```shell
   git clone <repository-url>
   ```
2. Create a Virtual Environement:

   ```shell
   python -m venv venv
   cd venv
   ```
3. Activate Virtual Environment:

   ```shell
   Windows: ./Scripts/activate
   ```
4. Install the required Python packages:

   ```shell
   pip install -r requirements.txt
   ```
5. Configure the database settings in `NextLabAssignment/settings.py`.
6. Apply migrations:

   ```shell
   python manage.py makemigrations
   python manage.py migrate
   ```
7. Create a superuser for admin access:

   ```shell
   python manage.py createsuperuser
   ```
8. Run the development server:

   ```shell
   python manage.py runserver
   ```

## Usage

### Admin Access

1. Access the admin panel at `http://localhost:8000/admin/` and log in with the superuser credentials created in step 5 of the installation.
2. Use the admin panel to add Android apps and set download points.

### User Access

1. Use the API endpoints for user registration, profile management, screenshot uploads, and app listings as described in the [API Endpoints](#api-endpoints) section.

## API Endpoints

The API provides the following endpoints:

- Admin Login: `/admin/login/` (POST)
- Add Android App: `/admin/apps/` (POST)
- User Signup: `/users/signup/` (POST)
- User Login: `/users/login/` (POST)
- User Profile: `/users/profile/` (GET)
- User Upload Screenshot: `/users/upload-screenshot/` (POST)
- List Apps and Points: `/apps/` (GET)

For detailed API documentation, you can visit the Swagger documentation at `http://localhost:8000/docs/`.

## Authentication

The API uses token-based authentication for both admin and regular users. To authenticate, include the token in the request's `Authorization` header as `Token <token-key>`.

## Documentation

API documentation is automatically generated using Swagger. You can access it at `http://localhost:8000/docs/`. The documentation includes detailed information about each endpoint, request/response examples, and data formats.

## Contributing

Contributions to this project are welcome. If you'd like to make improvements, please follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bug fix.
3. Make your changes and submit a pull request.

Please ensure your code follows best practices and includes appropriate tests.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

# Problem Set 3

##### Please answer the below questions:

A. Write and share a small note about your choice of system to schedule periodic tasks (such as downloading a list of ISINs every 24 hours). Why did you choose it? Is it reliable enough; Or will it scale? If not, what are the problems with it? And, what else would you recommend to fix this problem at scale in production?

Subject: Choosing a Task Scheduling System

Dear NextLabs,

I wanted to share some insights about our choice of a system for scheduling periodic tasks, such as downloading a list of ISINs every 24 hours. After careful consideration, we opted for Cron Jobs to handle this task. Here's why:

1. **Reliability:** Cron Jobs have been a staple in Unix-like operating systems for decades. They are renowned for their reliability in executing scheduled tasks. They are simple to set up and rarely fail if configured correctly.
2. **Scalability:** Cron Jobs are efficient for managing a moderate number of periodic tasks. They can scale well for many use cases, especially when you have a limited number of scheduled jobs like the daily ISIN list download. However, for more complex scheduling needs or when dealing with an extensive number of tasks, other solutions might be more appropriate.

Nonetheless, Cron Jobs do have their limitations:

1. **Limited Flexibility:** Cron Jobs are best suited for fixed, time-based scheduling. If you need more advanced scheduling options, like dynamic schedules based on events or dependencies, a more versatile solution may be required.
2. **Centralized Management:** Cron Jobs can become challenging to manage at scale when you have numerous servers or microservices, as they require manual configuration on each system.

To address these limitations and ensure seamless task scheduling at scale in a production environment, consider implementing a dedicated task scheduler or job orchestration system like Apache Airflow, Celery, or Kubernetes Cron Jobs. These systems offer centralized management, advanced scheduling features, and better scalability for complex workflows.

In summary, while Cron Jobs are a reliable and straightforward choice for basic periodic task scheduling, evaluating the specific needs of your system is crucial. If you anticipate scalability and complex scheduling requirements, it might be worthwhile to explore more robust solutions to optimize your production processes.

If you have any questions or need further assistance, please feel free to reach out.

Best Regards,
Soumya Ranjan Patra

###### B. In what circumstances would you use Flask instead of Django and vice versa?

Flask and Django are both popular Python web frameworks, but they cater to different use cases and preferences. Here are some circumstances in which you might choose one over the other:

**Use Flask when:**

1. **Microservices or Small Apps:** Flask is a micro-framework, making it an excellent choice for building small to medium-sized applications, APIs, or microservices. It provides flexibility without imposing a lot of structure.
2. **Simplicity:** If you prefer a minimalistic approach and want to keep your codebase as simple as possible, Flask is a great choice. You can add only the components you need, making it easy to understand and maintain.
3. **Customization:** Flask allows developers to have more control over the components they use. You can choose your preferred libraries, databases, and extensions, giving you the freedom to create a highly customized solution.
4. **Learning Curve:** Flask has a shorter learning curve, making it accessible for developers who want to get started quickly and gradually build their expertise in web development.

**Use Django when:**

1. **Rapid Development:** Django follows the "batteries-included" philosophy, providing a comprehensive set of tools and libraries for common web development tasks. It's ideal for quickly building robust, feature-rich applications.
2. **Large and Complex Projects:** For enterprise-level or large-scale projects with complex requirements, Django's built-in features for authentication, ORM, admin interface, and more can significantly speed up development and reduce boilerplate code.
3. **Convention Over Configuration:** Django enforces a "Django way" of doing things, which can be beneficial for teams by standardizing project structure and best practices. It helps maintain consistency in large development teams.
4. **Security:** Django has built-in security features like protection against common web vulnerabilities (Cross-Site Scripting, SQL Injection, Cross-Site Request Forgery), making it a preferred choice for applications with stringent security requirements.
5. **Admin Interface:** Django includes a powerful admin interface for managing application data, which can save a lot of development time when building content management systems or other data-driven applications.