# Generated by Django 4.2.6 on 2023-10-24 19:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('admin_panel', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='androidapp',
            old_name='name',
            new_name='app_name',
        ),
    ]
