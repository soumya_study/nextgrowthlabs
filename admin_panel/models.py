from django.db import models


class AndroidApp(models.Model):
    app_name = models.CharField(max_length=255)
    points_earned = models.PositiveIntegerField()