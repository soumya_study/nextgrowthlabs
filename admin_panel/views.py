from rest_framework import viewsets

from .models import AndroidApp
from .serializers import AndroidAppSerializer


class AndroidAppViewSet(viewsets.ModelViewSet):
    queryset = AndroidApp.objects.all()
    serializer_class = AndroidAppSerializer